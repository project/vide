About
=====
Integrates the Vide library into Drupal.

Current Options
---------------
Allows you to use Vide in a few different ways

- As a library to be used with any other theme or module by calling vide_add() (N.B. You may also use libraries_load('vide') or drupal_add_library('vide', 'vide'), but only if you want to control everything manually).
- Integrates with Fields (vide_fields)
- Adds a Views display mode (vide_views)

About Vide
----------------

Library available at https://github.com/VodkaBears/Vide


Easy as hell jQuery plugin for video backgrounds.

- All modern desktop browsers are supported.
- IE9+
- iOS plays video from a browser only in the native player. So video for iOS is disabled, only fullscreen poster will be used.
- Some android devices play video, some not — go figure. So video for android is disabled, only fullscreen poster will be used.

Installation
============

Dependencies
------------

- [Libraries API 2.x](http://drupal.org/project/libraries)
- [Vide Library](https://github.com/vodkabears/vide)

Tasks
-----

1. Download the vide library from https://github.com/VodkaBears/Vide/releases/tag/0.5.1
2. Unzip the file and rename the folder to "vide" (pay attention to the case of the letters)
3. Put the folder in a libraries directory
    - Ex: sites/all/libraries
4. The following files are required (last file is required for javascript debugging)
    - jquery.vide.min.js
    - jquery.vide.js
5. Ensure you have a valid path similar to this one for all files
    - Ex: sites/all/libraries/vide/jquery.vide.min.js

That's it!

Drush Install
----------

You can use a simple Drush command to install the Vide library.

    drush vide-plugin

Drush Make
----------

You can also use Drush Make to download the library automatically. Simply copy/paste the 'vide.make.example' to 'vide.make' or copy the contents of the make file into your own make file.

Usage
======

Option Sets
-----------

No matter how you want to use Vide, you need to define "option sets" to tell Vide how you want it to display. An option set defines all the settings for displaying the video. Things like volume, playback speed, additional classes, etc... You can define as many option sets as you like and on top of that they're all exportable! Which means you can carry configuration of your Vide instances from one site to the next or create features.

Go to admin/config/media/vide

From there you can edit the default option set and define new ones. These will be listed as options in the various forms where you setup Vide to display. 

Debugging
---------

You can toggle the development version of the library in the administrative settings page. This will load the production version of the library. Uncheck this when moving to a production site to load the smaller compressed version.

Export API
==========

You can export your Vide option presets using CTools exportables. So either using the Bulk Export module or Features.

External Links
==============

- [README Documentation for Vide](https://github.com/VodkaBears/Vide/blob/master/README.md)