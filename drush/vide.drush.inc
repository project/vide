<?php

/**
 * @file
 *   drush integration for vide.
 */

/**
 * The Vide plugin URI.
 */
define('VIDE_DOWNLOAD_URI', 'https://github.com/VodkaBears/Vide/archive/0.5.1.zip');
define('VIDE_DOWNLOAD_PREFIX', 'Vide-');

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * See `drush topic docs-commands` for a list of recognized keys.
 *
 * @return array
 *   An associative array describing your command(s).
 */
function vide_drush_command() {
  $items = array();

  // the key in the $items array is the name of the command.
  $items['vide-plugin'] = array(
    'callback' => 'drush_vide_plugin',
    'description' => dt('Download and install the Vide plugin.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap.
    'arguments' => array(
      'path' => dt('Optional. A path where to install the Vide plugin. If omitted Drush will use the default location.'),
    ),
    'aliases' => array('videplugin'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param string $section
 *   A string with the help section (prepend with 'drush:')
 *
 * @return string
 *   A string with the help text for your command.
 */
function vide_drush_help($section) {
  switch ($section) {
    case 'drush:vide-plugin':
      return dt('Download and install the Vide plugin from vodkabears.github.io/vide, default location is sites/all/libraries.');
  }
}

/**
 * Implements drush_MODULE_pre_pm_enable().
 */
function drush_vide_pre_pm_enable() {
  $modules = drush_get_context('PM_ENABLE_MODULES');
  if (in_array('vide', $modules) && !drush_get_option('skip')) {
    drush_vide_plugin();
  }
}

/**
 * Command to download the Vide plugin.
 */
function drush_vide_plugin() {
  $args = func_get_args();
  if (!empty($args[0])) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the zip archive
  if ($filepath = drush_download_file(VIDE_DOWNLOAD_URI)) {
    $filename = basename($filepath);
    $dirname = VIDE_DOWNLOAD_PREFIX . basename($filepath, '.zip');

    // Remove any existing Vide plugin directory
    if (is_dir($dirname) || is_dir('vide')) {
      drush_delete_dir($dirname, TRUE);
      drush_delete_dir('vide', TRUE);
      drush_log(dt('A existing Vide plugin was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the zip archive
    drush_tarball_extract($filename);

    // Change the directory name to "vide" if needed.
    if ($dirname != 'vide') {
      drush_move_dir($dirname, 'vide', TRUE);
      $dirname = 'vide';
    }
  }

  if (is_dir($dirname)) {
    drush_log(dt('Vide plugin has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the Vide plugin to @path', array('@path' => $path)), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
