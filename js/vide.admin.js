/**
* @file
* vide.admin.js
*
* Provides general enhancements to the admin interface.
*/

var Drupal = Drupal || {};

(function($, Drupal){
	"use strict";

	Drupal.behaviors.videAdmin = {
		attach: function(context, settings) {

			// Add range slider for Volume.
      $(".volume-wrap", context).slider({
        range: "min",
        value: 1,
        step: 0.05,
        min: 0,
        max: 1,
        slide: function(event, ui) {
          $("#edit-volume").val(ui.value);
        }
      });
      // Add range slider for playback rate.
      $(".playback-wrap", context).slider({
        range: "min",
        value: 1,
        step: 0.5,
        min: 0.5,
        max: 2,
        slide: function(event, ui) {
          $("#edit-playbackrate").val(ui.value);
        }
      });

		}
	};

})(jQuery, Drupal);
