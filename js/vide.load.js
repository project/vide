(function($) {

  // Behavior to load Vide
  Drupal.behaviors.vide = {
    attach: function(context, settings) {
      var sliders = [];
      if ($.type(settings.vide) !== 'undefined' && $.type(settings.vide.instances) !== 'undefined') {

        for (id in settings.vide.instances) {

          if (settings.vide.optionsets[settings.vide.instances[id]] !== undefined) {
            if (settings.vide.optionsets[settings.vide.instances[id]].asNavFor !== '') {
              // We have to initialize all the sliders which are "asNavFor" first.
              _vide_init(id, settings.vide.optionsets[settings.vide.instances[id]], context);
            } else {
              // Everyone else is second
              sliders[id] = settings.vide.optionsets[settings.vide.instances[id]];
            }
          }
        }
      }
      // Slider set
      for (id in sliders) {
        _vide_init(id, settings.vide.optionsets[settings.vide.instances[id]], context);
      }
    }
  };

  /**
   * Initialize the Vide instance
   */

  function _vide_init(id, optionset, context) {
    $('#' + id, context).once('vide', function() {
      // Remove width/height attributes
      // @todo load the css path from the settings
      $(this).find('ul.slides > li > *').removeAttr('width').removeAttr('height');

      if (optionset) {
        // Add events that developers can use to interact.
        $(this).vide($.extend(optionset, {
          start: function(slider) {
            slider.trigger('start');
          },
          before: function(slider) {
            slider.trigger('before');
          },
          after: function(slider) {
            slider.trigger('after');
          },
          end: function(slider) {
            slider.trigger('end');
          },
          added: function(slider) {
            slider.trigger('added');
          },
          removed: function(slider) {
            slider.trigger('removed');
          }
        }));
      } else {
        $(this).vide();
      }
    });
  }

}(jQuery));
