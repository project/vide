<?php
/**
 * @file
 * Administrative page callbacks for the vide module.
 */

/**
 * Submit handler for adding a new option set.
 */
function vide_form_optionset_add_submit($form, &$form_state) {
  $optionset = vide_optionset_create(array('name' => $form_state['values']['name'], 'title' => $form_state['values']['title']));

  $saved = vide_optionset_save($optionset, TRUE);

  if ($saved) {
    drupal_set_message(t('Option set %name was created.', array('%name' => $optionset->name)));
    $form_state['redirect'] = 'admin/config/media/vide/edit/' . $optionset->name;
  }
  else {
    drupal_set_message(t('Failed to create option set. Please verify your settings.'), 'error');
  }
}

/**
 * Defines the form elements used to edit the Vide library options
 *
 * @param array $options [optional]
 *  Pass in a set of default values for the options
 * @return array
 *  Returns the option set array
 */
function vide_option_elements($options = array()) {
  $form = array();

  // General Vide settings
  $form['vide_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vide options'),
    '#description' => t('Various settings for Vide.'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );
  $form['vide_options']['volume'] = array(
    '#type' => 'textfield',
    '#title' => t('Volume'),
    '#description' => t('HTML5 Playback rate. 1 = normal, 0.5 = half speed, 2 = double speed.'),
    '#default_value' => ($volume = $options['volume']) ? $volume : 1,
    '#element_validate' => array('_vide_validate_positive_integer'),
    '#field_prefix' => '<div class="volume-wrap"></div>',
    '#disabled' => TRUE,
    '#size' => 4,
  );
  $form['vide_options']['playbackRate'] = array(
    '#type' => 'textfield',
    '#title' => t('Playback Rate'),
    '#description' => t('HTML5 Playback rate. 1 = normal, 0.5 = half speed, 2 = double speed.'),
    '#default_value' => ($playbackRate = $options['playbackRate']) ? $playbackRate : 1,
    '#element_validate' => array('element_validate_number'),
    '#field_prefix' => '<div class="playback-wrap"></div>',
    '#disabled' => TRUE,
    '#size' => 4,
  );
  $form['vide_options']['muted'] = array(
    '#type' => 'checkbox',
    '#title' => t('Muted'),
    '#default_value' => ($muted = $options['muted']) ? $muted : 1,
  );
  $form['vide_options']['loop'] = array(
    '#type' => 'checkbox',
    '#title' => t('Loop'),
    '#default_value' => ($loop = $options['loop']) ? $loop : 1,
  );
  $form['vide_options']['autoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autoplay'),
    '#default_value' => ($autoplay = $options['autoplay']) ? $autoplay : 1,
  );
  $form['vide_options']['position'] = array(
    '#type' => 'textfield',
    '#title' => t('Position'),
    '#description' => t('Similar to the CSS `background-position` property.'),
    '#default_value' => ($position = $options['position']) ? $position : '50% 50%',
  );
  $form['vide_options']['posterType'] = array(
    '#type' => 'select',
    '#title' => t('Poster Type'),
    '#description' => t('Poster image type.'),
    '#default_value' => ($posterType = $options['posterType']) ? $posterType : 'detect',
    '#options' => array(
      'detect' => t('Auto detect'),
      'none' => t('No poster'),
      'jpg' => 'JPG',
      'png' => 'PNG',
      'gif' => 'GIF',
    ),
  );
  $form['vide_options']['resizing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto-resizing'),
    '#description' => t('The Vide plugin resizes if the window resizes. Read more on !github.', array('!github' => l('Github', 'https://github.com/VodkaBears/Vide#resizing'))),
    '#default_value' => ($resizing = $options['resizing']) ? $resizing : 1,
  );
  $form['vide_options']['bgColor'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#description' => t('Add a custom background color to the video. i.e, #FF0000, rgb(18,132,115), etc'),
    '#default_value' => ($bgColor = $options['bgColor']) ? $bgColor : 'transparent',
  );
  $form['vide_options']['className'] = array(
    '#type' => 'textfield',
    '#title' => t('Class name'),
    '#description' => t('Custom class name for the Vide wrapper. Separate multiple classes with a comma.'),
    '#default_value' => ($className = $options['className']) ? $className : '',
  );
  dpm($options);

  return $form;
}

/**
 * Form builder; Form to edit a given option set.
 */
function vide_form_optionset_edit($form, &$form_state) {
  $module_path = drupal_get_path('module', 'vide');
  dpm($form_state);
  if (empty($form_state['optionset'])) {
    $optionset = vide_optionset_create();
  }
  else {
    $optionset = $form_state['optionset'];
  }

  // Title
  $form['title'] = array(
    '#type' => 'textfield',
    '#maxlength' => '255',
    '#title' => t('Title'),
    '#description' => t('A human-readable title for this option set.'),
    '#required' => TRUE,
    '#default_value' => $optionset->title,
  );
  $form['name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => '255',
    '#machine_name' => array(
      'source' => array('title'),
      'exists' => 'vide_optionset_exists',
    ),
    '#required' => TRUE,
    '#default_value' => $optionset->name,
  );

  // Options Vertical Tab Group table
  $form['options'] = array(
    '#type' => 'vertical_tabs',
  );

  // Add admin related CSS / JS.
  $form['#attached']['library'][] = array('system', 'ui.slider');
  $form['#attached']['js'][] = $module_path . '/js/vide.admin.js';
  $form['#attached']['css'][] = $module_path . '/css/vide.admin.css';

  // Submit handler.
  $form['#submit'][] = 'vide_form_settings_submit';

  $default_options = vide_option_elements($optionset->options);
  // Add the options to the vertical tabs section
  foreach ($default_options as $key => $value) {
    $form['options'][] = $value;
  }

  return $form;
}

/**
 * Validate a form element that should have an integer value.
 */
function _vide_validate_positive_integer($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name must be a positive integer.', array('%name' => $element['#title'])));
  }
}

/**
 * Validate a form element that should have a number as value.
 */
function _vide_validate_number($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && !is_numeric($value)) {
    form_error($element, t('%name must be a number.', array('%name' => $element['#option_name'])));
  }
}

/**
 * Form builder; Form to delete a given option set.
 */
function vide_optionset_form_delete($form, &$form_state, $optionset) {
  $form_state['optionset'] = &$optionset;

  // Deleting an export in code will revert it.
  $op = ($optionset->export_type & EXPORT_IN_CODE) ? 'Revert' : 'Delete';

  return confirm_form(
    $form,
    t('Are you sure you want to @action the option set %name?', array('@action' => t(drupal_strtolower($op)), '%name' => $optionset->name)),
    'admin/config/media/vide',
    NULL,
    t($op),  t('Cancel')
  );
}

/**
 * Submit handler for deleting an option set.
 */
function vide_optionset_form_delete_submit($form, &$form_state) {
  $optionset = &$form_state['optionset'];
  $op = ($optionset->export_type & EXPORT_IN_CODE) ? 'reverted' : 'deleted';

  ctools_include('export');
  ctools_export_crud_delete('vide_optionset', $optionset);

  drupal_set_message(t('Option set %name was ' . $op . '.', array('%name' => $optionset->name)));
  $form_state['redirect'] = 'admin/config/media/vide';
}


/**
 * Form builder; Form for advanced module settings.
 */
function vide_form_settings() {
  $form = array();

  $form['library'] = array(
    '#type' => 'fieldset',
    '#title' => 'Library',
  );

  // Debug mode toggle
  $form['library']['vide_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug mode'),
    '#description' => t('Load the human-readable version of the vide library.'),
    '#default_value' => variable_get('vide_debug', FALSE),
    '#access' => user_access('administer vide'),
  );

  return system_settings_form($form);
}

/**
 * Submit handler for the advanced module settings.
 */
function vide_form_settings_submit($form, &$form_state) {
  // Do nothing for now
  dpm($form_state);
  $input = $form_state['input'];
}

function _vide_validate_selector($element, &$form_state) {
  // @todo
  // @see form_error()
  return TRUE;
}
