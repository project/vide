<?php

/**
 * @file
 * API documentation for vide
 */

/**
 * By design, vide should be entirely configurable from the web interface.
 * However some implementations may require to access the vide library
 * directly by using vide_add().
 *
 * Here are some sample uses of vide_add().
 */

/**
 * This call will look for an HTML element with and id attribute of "my_image_list"
 * and initialize vide on it using the option set named "default".
 */
vide_add('my_image_list', 'default');

/**
 * You also have the option of skipping the option set parameter if you want
 * to run with the library defaults or plan on adding the settings array
 * into the page manually using drupal_add_js().
 */
vide_add('my_image_list');

/**
 * Finally, you can simply have Drupal include the library in the list of
 * javascript libraries. This method would assume you would take care of
 * initializing a vide instance in your theme or custom javascript
 * file.
 *
 * Ex: $('#slider').vide();
 */
vide_add();