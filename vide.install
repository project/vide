<?php
/**
 * @file
 * Installation actions for vide
 */

/**
 * Implements hook_schema().
 */
function vide_schema() {
  $schema = array();

  $schema['vide_optionset'] = array(
    'description' => 'Store option sets for Vide instances.',
    'export' => array(
      'key' => 'name',
      'identifier' => 'preset',
      'default hook' => 'default_vide_presets',
      'api' => array(
        'owner' => 'vide',
        'api' => 'default_vide_preset',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'name' => array(
        'description' => 'The machine-readable option set name.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => 'The human-readable title for this option set.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'options' => array(
        'description' => 'The options array.',
        'type' => 'blob',
        'size' => 'big',
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('name'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 *
 * Adds a 'default' option set for fresh installs.
 */
function vide_install() {
  $optionset = vide_optionset_create();
  $optionset->title = t('Default');
  $optionset->name = 'default';
  vide_optionset_save($optionset, TRUE);
}

/**
 * Implements hook_uninstall().
 */
function vide_uninstall() {
  variable_del('vide_debug');
  variable_del('vide_version');
}

/**
 * Implements hook_requirements().
 */
function vide_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break at install time
  $t = get_t();

  // Check to see if the vide library is available
  if ($phase == 'runtime') {
    $requirements['vide'] = array(
      'title' => $t('Vide'),
      // @todo have the version automatically detected
      'description' => $t('Version 0.5.0 installed'),
      'severity' => REQUIREMENT_OK,
    );
    _vide_requirements_library_installed($requirements);
  }
  return $requirements;
}

/**
 * Check if the library is available
 *
 * @param array $requirements
 *  Requirements definition
 */
function _vide_requirements_library_installed(&$requirements) {
  $t = get_t();

  $path = libraries_get_path('vide');
  $installed = file_exists($path . '/dist/jquery.vide.min.js') && file_exists($path . '/dist/jquery.vide.js');

  // Check the results of the test
  if (!$installed) {
    $requirements['vide']['description'] = $t('Vide library not found. Please consult the README.txt for installation instructions.');
    $requirements['vide']['severity'] = REQUIREMENT_ERROR;
    $requirements['vide']['value'] = $t('Vide library not found.');
    return;
  }

  $js = file_exists($path . '/dist/jquery.vide.min.js') ? fopen($path . '/dist/jquery.vide.min.js', 'r') : fopen($path . '/dist/jquery.vide.js', 'r');
  $header = fread($js, 64);
  $matches = array();
  if (preg_match("/ v([0-9]+)\.([0-9]+)\.([0-9]+)/", $header, $matches)) {
    if (!($matches[1] == 0 and $matches[2] >= 4)) {
      $requirements['vide']['description'] = $t('Vide must be version 0.4 or higher.  Please consult the README.txt for installation instructions.');
      $requirements['vide']['severity'] = REQUIREMENT_WARNING;
      $requirements['vide']['value'] = $t('Incorrect version detected.');
      return;
    }
    else {
      // Combine all matches as complete version.
      $tmp_match = $matches;
      unset($tmp_match[0]);
      $version = implode('.', $tmp_match);
      variable_set('vide_version', $version);
      $requirements['vide']['description'] = $t('Version %version installed', array( '%version' => $version));
      $requirements['vide']['value'] = $t('Vide library installed.');
      return;
    }
  }
  else {
    $requirements['vide']['description'] = $t('Vide version could not be determined.  Please consult the README.txt for installation instructions.');
    $requirements['vide']['severity'] = REQUIREMENT_WARNING;
    $requirements['vide']['value'] = $t('Unable to detect version.');
  }
}

// @todo add hook_update_N function to migrate old option set data to new values
